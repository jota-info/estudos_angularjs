angular.module("listaTelefonica").controller("listaTelefonicaController",
    function ($scope){
      $scope.app = "Lista Telefônica";
      $scope.contatos = [
        {nome: "macGyver", telefone: "+550144734441441"},
        {nome: "Tio San", telefone: "+550144734496417"},
        {nome: "James Bond", telefone: "+550144734321417"},
        {nome: "Sherlock Holmes", telefone: "@+?}^?$##@%!@#17"},
      ];
      $scope.addContato = function(contato) {
        $scope.contatos.push(contato);
        delete $scope.contato;
      };
      $scope.delContato = function(contatos) {
        $scope.contatos = contatos.filter(function(contato){
          if (!contato.selecionado) return contato;
        });
      };
});
